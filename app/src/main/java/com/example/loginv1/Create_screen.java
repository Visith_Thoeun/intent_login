package com.example.loginv1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Create_screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_screen);

        ImageView btn_back = (ImageView)findViewById(R.id.back01);

        View.OnClickListener back_to_Page1 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent CS = new Intent(Create_screen.this,MainActivity.class);
                startActivity(CS);
            }
        };
        btn_back.setOnClickListener(back_to_Page1);
    }
}
package com.example.loginv1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    // go to create account
       TextView txt = (TextView) findViewById(R.id.to_create);
       View.OnClickListener go_screen_create = new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent LG = new Intent(MainActivity.this,Create_screen.class);
               startActivity(LG);
           }
       };
       txt.setOnClickListener(go_screen_create);


        // if authentication success!
        final EditText user = (EditText) findViewById(R.id.user);
        final EditText pass = (EditText) findViewById(R.id.pass);
        Button login = (Button) findViewById(R.id.login);

        View.OnClickListener auth = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.getText().toString().equals("visith") && pass.getText().toString().equals("123")){
                    Intent authed = new Intent(getApplicationContext(), Authenticated.class);
                    startActivity(authed);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder
                        .setTitle("Error!")
                        .setMessage("email or password is incorrect")
                        .setCancelable(false)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                    ;
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            }
        };
        login.setOnClickListener(auth);

    }




}